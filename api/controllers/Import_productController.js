/**
 * Import_productController
 *
 * @description :: Server-side logic for managing import_products
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  list: function (req, res) {
    function success(err, results) {
      sails.log.info("# of register: ", results.length);
      res.send(results);
    };

    var query = "select * from magento.catalog_product_entity;"
    Import_product.query(query, success);
  },

};

