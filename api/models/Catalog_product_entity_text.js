/**
* Catalog_product_entity_text.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  schema: true,
  tableName: 'catalog_product_entity_text',
  autoPK:false,
  autoCreatedAt: false,
  autoUpdatedAt: false,
  attributes: {
    'value_id':{
      type: 'int',

    },
    'attribute_id':   'int',
    'store_id':       'int',
    'entity_id':      'int',
    'value':          'string',
  },
};

