/**
* Catalog_product_website.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  schema: true,
  tableName: 'catalog_product_website',
  autoPK:false,
  autoCreatedAt: false,
  autoUpdatedAt: false,
  attributes: {
    'product_id':{
      type: 'int',

    },
    'website_id':   'int',

  },
};

