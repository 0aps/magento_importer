/**
* Import_product.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  schema: true,
  tableName: 'catalog_product_entity',
  autoPK:false,
  autoCreatedAt: false,
  autoUpdatedAt: false,
  attributes: {
    'entity_id':{
        type: 'int',
        primaryKey: true,
        autoIncrement:true,
    },
    'attribute_set_id':     'int',
    'type_id':              'string',
    'sku':                  'string',
    'has_options':          'int',
    'required_options':     'int',
    'created_at':           'datetime',
    'updated_at':           'datetime',
  },
};

