/**
 * Created by 0aps on 12/01/16.
 */


(function () {
  'use strict';

  var app = angular.module('asterisk_app', []);
  app.factory("ImportFactory", ImportFactory);
  app.controller("DashboardController", DashboardController);

  function ImportFactory($http){
    return {
      sendData: sendData,
    };

    function sendData(endpoint, query){
      var querySet;
      var endpoint = endpoint;

      querySet = $http.post(endpoint, query);

      return querySet.then(success)
        .catch(fail);

      function success(result) {
        return result.data;
      };

      function fail(err) {
        console.error(err);
      };
    };

  };

  function DashboardController($scope, ImportFactory){
    var vm = $scope;
    vm.product_entries = [];
    var endpoints = ["import_product",]
                     // "catalog_product_entity_text",
                     // "catalog_product_entity_varchar",
                     // "catalog_product_entity_decimal",
                     // "catalog_product_website"];



    var template = {
      "import_product": {"attribute_set_id": 4,
                                  "type_id": "simple",
                                  "sku": makeid(),
                                  "has_options": 0,
                                  "required_options": 0
                                },
      "catalog_product_entity_text": {
                                      "attribute_id": 72,
                                      "store_id": 0,
                                      "entity_id": 0, //associate with last one
                                      "value": "testdescription" //description
                                    },
      "catalog_product_entity_varchar": {
                                      "attribute_id": 70,
                                      "store_id": 0,
                                      "entity_id": 0, //associate with last one
                                      "value": "testname" //name
                                    },
      "catalog_product_entity_decimal": {
                                      "attribute_id": 74,
                                      "store_id": 0,
                                      "entity_id": 0, //associate with last one
                                      "value":  25 //price
                                    },
      "catalog_product_website":{
                                     "product_id": 0, //entity id
                                     "website_id": 1
      }

    };

    vm.import_products = function () {
      vm.product = {};
      vm.in_progress = 0;
      vm.seconds = 0;
      setInterval(function () {
        vm.seconds ++;
      }, 1000);
      for(var i = 0; i<1000; ++i) {
        create_product("import_product", 0, {});
      }
    };

    function create_product(endpoint, i, entity) {

      if(vm.in_progress == 1000){
        clearInterval(vm.seconds);
      };
      if(i == endpoints.length) {
        return;
      }

      if(i > 0 && i < 4){
        template[endpoint].entity_id = entity.entity_id;
        template[endpoint].value = getProductValue(i, entity);
      }
      if(i == 4){
        template[endpoint].product_id = entity.entity_id;
      }

      ImportFactory.sendData(endpoint, template[endpoint]).then(function (result) {
        vm.in_progress++;
         console.log("Executing i with", i);
         console.log("Executing result with", result);
          if(i == 0){
            entity.entity_id = result.entity_id;
            entity.sku = result.sku;
          }
          create_product(endpoints[i+1], i+1, entity)
      });
    }

    function makeid(){
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

      for( var i=0; i < 5; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

      return text;
    };

    function getProductValue(i, entity) {
        if(i == 1) return entity.description || "test_description"+entity.sku;
        if(i == 2) return entity.name || "test_name"+entity.sku;
        if(i == 3) return entity.price || Math.floor(Math.random() * 100) + 1;
    }

  };

})();
